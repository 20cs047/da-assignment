import pandas as pd
import math
n=12
u=1009.4499999999999
sd=368.40030520338826
print("\n\nH0: it is taken from a large population \n")
print("H1: it is not taken from a large population \n\n")
df1=pd.read_csv("new.csv")
m=df1["1980"].mean()
sd=df1["1980"].std()
se2=sd/math.sqrt(n)
z= (m - u) / se2
print("Calculated z value =",z)
if z < 1.96 and z > -1.96:
        print("\nretain H0, this sample is taken from large population")
else:
    print("\nreject H0,this sample is not taken from large sample")
